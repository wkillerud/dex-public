// Entry for Karma. See karma.conf.js
// require.context(directory, useSubdirectories = false, regExp = /^\.\//)
var appContext = require.context('./app', true, /-test.js$/);
appContext.keys().forEach(appContext);

var serverContext = require.context('./server', true, /-test.js$/);
serverContext.keys().forEach(serverContext);