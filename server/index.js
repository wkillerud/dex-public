var express = require('express');
var fs = require('fs');
var webpack = require('webpack');
var config = require('../webpack/webpack.config.dev.js');
var app = express();
var compiler = webpack(config);

var isDev = process.env.NODE_ENV === 'development';

if (isDev) {
    app.use(require('webpack-dev-middleware')(compiler, {
        noInfo: true,
        publicPath: config.output.publicPath
    }));

    app.use(require('webpack-hot-middleware')(compiler));
}


// Enable runtime transpilation to use ES6/7 in node


var babelrc;

try {
    babelrc = JSON.parse(fs.readFileSync('./.babelrc.json'));
} catch (err) {
    console.error('==>     ERROR: Error parsing .babelrc.');
    console.error(err);
    throw err;
}

require('babel-register')(babelrc);
// Anything beyond here should be ES6 and beyond
require('./config/app').default(app);
require('./config/routes').default(app);

app.listen(app.get('port'));
