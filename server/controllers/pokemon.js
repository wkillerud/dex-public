import { db } from '../../db/pokedex';
import { isNumeric, isNumberWithinLimit } from '../utils/numbers';
import { serverError, notFound } from '../utils/responses';

export function all(req, res) {
    const defaultLimit = 10;
    const maxLimit = 200;

    const limit = isNumberWithinLimit(req.query.limit, maxLimit) ?
        Number(req.query.limit) :
        defaultLimit;

    const offset = isNumeric(req.query.offset) ? Number(req.query.offset) : 0;
    
    let querySelector = '';    
    const selectorParameters = {
        $limit: limit,
        $offset: offset
    };
    
    if(req.query.q) {
        querySelector = 'WHERE pokemon_species.identifier LIKE $q';
        selectorParameters.$q = `${req.query.q}%`;
    }
    
    const selector = 'SELECT pokemon_species.*, GROUP_CONCAT(types.identifier) AS type FROM pokemon_species ' + 
        'INNER JOIN pokemon_types ON pokemon_species.id = pokemon_types.pokemon_id ' + 
        'INNER JOIN types ON pokemon_types.type_id = types.id ' +  
        `${querySelector} ` + 
        'GROUP BY pokemon_species.identifier ' +
        'LIMIT $limit OFFSET $offset';

    db.all(selector, selectorParameters, (err, rows) => {
        if (err) {
            serverError(err, selector, selectorParameters);
        } else {
            res.send({
                result: rows,
                metadata: {
                    resultset: {
                        count: rows.length,
                        offset: offset,
                        limit: limit
                    }
                }
            });
        }
    });
}

export function single(req, res) {
    const name = req.params.name;

    let selector = 'SELECT ' +
        'pokemon_species.id, ' +
        'pokemon_species.identifier, ' +
        'GROUP_CONCAT(types.identifier) AS type, ' +
        'pokemon_species.gender_rate, ' +
        'pokemon_species.capture_rate, ' +
        'pokemon_species.base_happiness, ' +
        'pokemon_species.has_gender_differences, ' +
        'pokemon_species_flavor_text.flavor_text, ' +
        'pokemon_habitats.identifier AS habitat, ' +
        'pokemon_shapes.identifier AS shape, ' +
        'pokemon_species_evolution.identifier AS evolves_from ' +
        'FROM pokemon_species LEFT OUTER JOIN pokemon_species AS pokemon_species_evolution ON pokemon_species.evolves_from_species_id = pokemon_species_evolution.id ' + 
        'INNER JOIN pokemon_species_flavor_text ON pokemon_species.id = pokemon_species_flavor_text.species_id ' +
        'INNER JOIN pokemon_habitats ON IFNULL(pokemon_species.habitat_id, 5) = pokemon_habitats.id ' +
        'INNER JOIN pokemon_shapes ON pokemon_species.shape_id = pokemon_shapes.id ' +
        'INNER JOIN pokemon_types ON pokemon_species.id = pokemon_types.pokemon_id ' + 
        'INNER JOIN types ON pokemon_types.type_id = types.id ' +
        'WHERE pokemon_species_flavor_text.language_id = 9 ' +
        'AND pokemon_species_flavor_text.version_id = 24 ' + 
        'AND pokemon_species.identifier = $name '+ 
        'GROUP BY pokemon_species.identifier';

    db.get(selector, { $name: name }, (err, row) => {
        if (err) {
            serverError(err, selector, name);
        } else {
            if(!row) {
                notFound(res, 'Found no Pokémon by that name');
            } else {
                res.send({
                    result: row
                });   
            }
        }
    });
}