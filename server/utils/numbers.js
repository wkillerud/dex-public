import { isNumber, isString } from 'lodash';

export function isNumberWithinLimit(val, limit) {
    if (!isNumber(limit)) {
        throw Error('The limit parameter is not a number. Unable to check if number is within its limits.');
    }

    try {
        let num = Number.parseInt(val, 10);
        return num <= limit;
    } catch (e) {
        return false;
    }
}

export function isNumeric(val) {
    if (isNumber(val)) {
        return true;
    } else if (isString(val) && val !== '') {
        try {
            let num = Number(val);
            if (Number.isNaN(num)) {
                return false;
            } else {
                return true;
            }
        } catch (e) {
            return false;
        }
    } else {
        return false;
    }
}  
