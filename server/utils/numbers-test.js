import expect from 'expect';
import { isNumberWithinLimit, isNumeric } from './numbers';

describe('Numbers utility', () => {

    describe('isNumberWithinLimit', () => {

        it('should throw an error if no limit was given', (done) => {
            try {
                isNumberWithinLimit(1);
            } catch (e) {
                done();
            }
        });

        it('should return false if the value is over the limit', () => {
            expect(isNumberWithinLimit(100, 1)).toBe(false);
        });

        it('should return true if the value is under or equal to the limit', () => {
            expect(isNumberWithinLimit(1, 2)).toBe(true);
            expect(isNumberWithinLimit(1, 1)).toBe(true);
        });

        it('should return false if the value is anything other than a number', () => {
            expect(isNumberWithinLimit(null, 10)).toBe(false);
            expect(isNumberWithinLimit('asdf', 10)).toBe(false);
            expect(isNumberWithinLimit(undefined, 10)).toBe(false);
            expect(isNumberWithinLimit({}, 10)).toBe(false);
            expect(isNumberWithinLimit(Infinity / Infinity, 10)).toBe(false); // NaN         
        });

    });
    
    describe('isNumeric', () => {

        it('should return false if the value is anything other than a Number or a String coercable by Number', () => {
            expect(isNumeric(false)).toBe(false);
            expect(isNumeric('false')).toBe(false);
            expect(isNumeric(null)).toBe(false);
            expect(isNumeric('asdf')).toBe(false);
            expect(isNumeric('1234asdf')).toBe(false);
            expect(isNumeric(undefined)).toBe(false);
            expect(isNumeric({})).toBe(false);
            expect(isNumeric(Infinity / Infinity)).toBe(false); // NaN      
        });
        
        it('should return false if given an empty string', () => {
            // Number('') === 0, so handle this special case
            expect(isNumeric('')).toBe(false);
        });
        
        it('should return true for Strings with only digits, and Numbers', () => {
            expect(isNumeric('123')).toBe(true);
            expect(isNumeric('9999999999999999999999999999999999999999999999999999999999999999')).toBe(true);
            expect(isNumeric(1e+64)).toBe(true);
            expect(isNumeric(123)).toBe(true);
            expect(isNumeric(Number.MAX_SAFE_INTEGER)).toBe(true);
            expect(isNumeric(Number.MAX_VALUE)).toBe(true);
            expect(isNumeric(Number.MIN_VALUE)).toBe(true);
            expect(isNumeric(Number.MIN_SAFE_INTEGER)).toBe(true);
        });

    });
});
