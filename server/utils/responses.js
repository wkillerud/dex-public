export function serverError(res, ...err) {
    console.error(err);
    
    res.status(500).send({
        error: {
            message: 'Server error' 
        }
    });
}

export function notFound(res, message = 'Resource not found') {
    res.status(404).send({ 
        error: {                                   
            message
        }
    });
}