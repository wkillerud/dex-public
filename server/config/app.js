import express from 'express';
import path from 'path';

export default function application(app) {
    app.set('port', (process.env.PORT || 8888));
    
    app.use('/', express.static(path.join(__dirname, '../..', 'public')));

    console.log('--------------------------');
    console.log('===>  Starting Server . . .');
    console.log('===>  Environment: ' + process.env.NODE_ENV);
    console.log('===>  Port: ' + app.get('port'));
}