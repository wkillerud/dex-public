import { dex } from '../routers/dex';
import * as App from '../../public/bundles/app.server';

export default function routes(app) {
    app.use('/dex/v1', dex);
    
    // Look for other routes in the React application to render the page server-side. 
    // If no route is configured in the React router, a 404 is sent as a response
    app.get('*', function (req, res) {
        App.default(req, res);
    });
}