import express from 'express';
import * as pokemon from '../controllers/pokemon';

export const dex = express.Router();

/**
 * @api {get} /dex/v1/pokemon.json[?q=:query[&limit=:limit[&offset=:offset]]]
 * @apiName Pokemon
 * @apiGroup Pokemon
 * @apiVersion 1.0.0
 * 
 * @apiParamExample Default
 *   // Returns the first page of 25 pokÃ©mon in the pokÃ©dex
 *     pokemon.json
 * 
 * * @apiParamExample Offset
 *   //Allows for pagination. Use the metadata object to keep track of your page position.  
 *       pokemon.json?offset=25
 * 
 * * @apiParamExample Limit 
 *   //Controls the number of the returned pokÃ©mon:
 *       pokemon.json?limit=5
 *
 *   //Maximum allowed is 200:
 *       pokemon.json?limit=200
 * 
 * @apiParamExample Query
 *   // Returns pokÃ©mon whose name starts with the query string
 *     pokemon.json?q=pidge
 * 
 * 
 * 
 * 
 * @apiSuccessExample Success-Response
 *     HTTP/1.1 200 OK
 *     {
 *       "results": [
 *         {
 *           "id": 1,
 *           "identifier": "bulbasaur",
 *           "generation_id": 1,
 *           "evolves_from_species_id": null,
 *           "evolution_chain_id": 1,
 *           "color_id": 5,
 *           "shape_id": 8,
 *           "habitat_id": 3,
 *           "gender_rate": 1,
 *           "capture_rate": 45,
 *           "base_happiness": 70,
 *           "is_baby": 0,
 *           "hatch_counter": 20,
 *           "has_gender_differences": 0,
 *           "growth_rate_id": 4,
 *           "forms_switchable": 0,
 *           "order": 1,
 *           "conquest_order": null
 *         },
 *         {
 *           "id": 2,
 *           ...
 *         }
 *       ],
 *       "metadata": 
 *       {
 *         "resultset": 
 *         {
 *           "count": 25,
 *           "offset": 0,
 *           "limit": 25
 *         }
 *       }
 *     }
 * 
 * @apiSuccessExample Query with no results
 *     HTTP/1.1 200 OK
 *     {
 *       "results": [],
 *       "metadata": 
 *       {
 *         "resultset": 
 *         {
 *           "count": 0,
 *           "offset": 0,
 *           "limit": 25
 *         }
 *       }
 *     }
 * 
 */
dex.get('/pokemon.json', pokemon.all);

/**
 * @api {get} /dex/v1/pokemon/:name.json
 * @apiName Pokemon details 
 * @apiGroup Pokemon
 * @apiVersion 1.0.0
 * 
 * @apiSuccessExample {json} Success-Response
 *     HTTP/1.1 200 OK
 *     {
 *       "results": 
 *       {
 *         "id": 1,
 *         "identifier": "bulbasaur",
 *         "generation_id": 1,
 *         "evolves_from_species_id": null,
 *         "gender_rate": 1,
 *         "capture_rate": 45,
 *         "base_happiness": 70,
 *         "has_gender_differences": 0,
 *         "flavor_text": "For some time after its birth, it grows by gaining\nnourishment from the seed on its back.",
 *         "habitat": "grassland",
 *         "shape": "quadruped"
 *       }
 *     }
 * 
 * @apiErrorExample {json} Not found
 *      HTTP/1.1 404 Not Found
 *      {
 *        "error": 
 *         {
 *          "message": "Found no PokÃ©mon by that name"
 *         }
 *      }
 */
dex.get('/pokemon/:name.json', pokemon.single);
