import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import createRoutes from 'routes.jsx';

// Grab the state from a global injected into
// server-generated HTML
const initialState = window.__INITIAL_STATE__;
const routes = createRoutes();

// Router converts <Route> element hierarchy to a route config:
// Read more https://github.com/rackt/react-router/blob/latest/docs/Glossary.md#routeconfig
render(
    <Router history={browserHistory}>
        {routes}
    </Router>, document.getElementById('app')
);
