import React, { Component, PropTypes } from 'react';
import 'less/main';

/*
 * React-router's <Router> component renders <Route>'s
 * and replaces `this.props.children` with the proper React Component.
 *
 * Please refer to `routes.jsx` for the route config.
 *
 * A better explanation of react-router is available here:
 * https://github.com/rackt/react-router/blob/latest/docs/Introduction.md
 */
const App = ({children}) => {
    return (
        <div>
            <header className="header">
                <a href="/dex">
                    <img src="/resources/app/icon.png" className="header__icon" />
                    <span className="header__text">_dex</span>
                </a>
            </header>
            
            {children}        
        </div>
    );
};

App.propTypes = {
    children: PropTypes.object
};

export default App;
