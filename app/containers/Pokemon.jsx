import axios from 'axios';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import PokemonType from '../components/PokemonType';

class Pokemon extends React.Component {
    state = {
        entry: {}
    }
    
    componentDidMount() {
        const id = this.props.params.identifier;

        axios({
            method: 'get',
            url: '/dex/v1/pokemon/' + id + '.json'
        }).then(res => 
            this.setState({ entry: res.data.result })
        );
    }
    
    render() {
        const entry = this.state.entry;
        if(!entry.id) {
            return (<div>Loading...</div>);
        }
        return (
            <section className="pokemon">                
                <div className="pokemon__profile">
                    <img src={'/resources/sprites/' + entry.id + '.png'} alt={'Image showing ' + entry.identifier} className="pokemon__profile-image" />
                    <h1>{entry.identifier}</h1>                    
                </div>

                <div className="pokemon__card">
                    <div className="pokemon__flavor-text">
                        {entry.flavor_text}
                    </div>
                </div>
                
                <div className="pokemon__card">
                    <div className="pokemon__profile-details">
                        <div>Type: <PokemonType entry={entry} /></div>
                        { entry.evolves_from ? <div>Evolves from: <a className="pokemon__evolves-from" href={'/dex/mon/' + entry.evolves_from}>{entry.evolves_from}</a></div> : '' }
                    </div>
                </div>
                
                
                <div className="pokemon__card external-links">
                    <ul>
                        <li>
                            <a className="external-links__link" href={'http://m.bulbapedia.bulbagarden.net/wiki/' + entry.identifier + '_%28Pok%C3%A9mon%29'}>
                                <span className="external-links__pokemon-name">{entry.identifier}</span> on Bulbapedia
                            </a>
                        </li>
                        <li>
                            <a className="external-links__link" href={'http://www.serebii.net/pokedex-xy/' + (entry.id < 10 ? '00' + entry.id : (entry.id < 100 ? '0' + entry.id : entry.id))  + '.shtml'}>
                                <span className="external-links__pokemon-name">{entry.identifier}</span> on Serebii
                            </a>
                        </li>
                    </ul>
                </div>
                
            </section>
        );
    }
}

export default Pokemon;
