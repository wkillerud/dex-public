import React from 'react';
import axios from 'axios';
import { browserHistory } from 'react-router';
import PokemonType from '../components/PokemonType';


class Search extends React.Component {
    
    state = {
        result:[]
    }
    
    render() {
        return (
            <section className="search">
                <div className="search__body">
                    <div className="search__input">
                        <label className="search__input-label" htmlFor="search-input">
                            Search the Pokédex
                        </label>
                        <input 
                            className="search__input-entry"
                            id="search-input"
                            type="search"
                            autoComplete="off"
                            autoCorrect="off"
                            autoFocus="true" 
                            maxLength="30"
                            spellCheck="false"
                            onChange={this.query.bind(this)}
                            onKeyDown={this.onKeyDown.bind(this)} />
                    </div>
                    
                    <div className="search__result-list">
                    {
                        this.state.result.map(pokemon => { 
                            return (
                                <a className="search__result" href={'/dex/mon/' + pokemon.identifier} key={pokemon.identifier}>
                                    <img className="search__result-icon" src={'/resources/icons/' + pokemon.id + '.png'} aria-hidden="true" />
                                    <span className="search__result-name">{pokemon.identifier}</span>
                                    <span className="search__result-type">
                                        <PokemonType entry={pokemon} />
                                    </span>
                                </a>
                            );
                        })
                    }
                    </div>
                    
                </div>
            </section>
        );   
    }
    
    onKeyDown(event) {
        if(event.key === 'Enter') {
            let mon = this.state.result[0];
            if(mon) {
                browserHistory.push('/dex/mon/' + mon.identifier);                                
            }
        }
    }
    
    query(event) {
        axios({
            method: 'get',
            url: '/dex/v1/pokemon.json',
            params: {
                q: event.currentTarget.value
            }
        }).then((res) => {
            this.setState({ result: res.data.result });
        });
    }
}

export default Search;
