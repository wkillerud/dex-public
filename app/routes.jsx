import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import App from 'containers/App';
import Search from 'containers/Search';
import Pokemon from 'containers/Pokemon';

export default () => {
    return (
        <Route path="/dex" component={App}>
            <IndexRoute component={Search} />
            <Route path="mon/:identifier" component={Pokemon} />
        </Route>
    );
};
