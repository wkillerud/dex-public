
import React from 'react';
import ReactDOMServer from 'react-dom/server';

class PokemonType extends React.Component {
    static propTypes = {
        entry: React.PropTypes.object.isRequired
    }
    
    render() {
        const entry = this.props.entry;        
        return (
            <span className="pokemon-type">
            {
                entry.type.split(',').map(type => {
                    return (
                        <img key={type} className="pokemon-type__image" src={'/resources/types/' + type + '.png'} alt={type}/>  
                    );
                })
            }  
            </span>
        );
    }
}

export default PokemonType;
