/*
 * Based on the template in Web Starter Kit : https://github.com/google/web-starter-kit/blob/master/app/index.html
 * To add to the config, add an object:
 * {
 *  type: 'link' | 'meta',
 *  sizes: 'widthxheight',
 *  rel: 'rel value'
 *  filename: <Name of your file'
 * }
 */

// Import all your needed files first (webpack will grab the url)
// import favicon from 'images/favicon.png';


const config = {
    title: '_dex',
    
    link: [
        { 'rel': 'stylesheet', 'href': '/bundles/main.css' },
        { 'rel': 'icon', 'href': '/resources/app/favicon.ico' },
        { 'rel': 'icon',  'sizes': '192x192', 'href': '/resources/app/chromeicon.png' },
        { 'rel': 'apple-touch-icon',  'sizes': '152x152', 'href': '/resources/app/safaricon.png' }
    ],
    meta: [
        { 'charset': 'utf-8' },
        // Setting IE=edge tells Internet Explorer to use the latest engine to render the page and execute Javascript
        { 'http-equiv': 'X-UA-Compatible', 'content': 'IE=edge' },
        //  Meta descriptions are commonly used on search engine result pages to display preview snippets for a given page.
        { 'name': 'description', 'content': 'A Pokédex for the trainer on the go' },
        // Mobile Safari introduced this tag to let web developers control the viewport's size and scale
        // The width property controls the size of the viewport, the initial-scale property controls
        // the zoom level when the page is first loaded
        { 'name': 'viewport', 'content': 'width=device-width, initial-scale=1' },
        // Add to homescreen for Chrome on Android
        { 'name': 'mobile-web-app-capable', 'content': 'yes' },
        // Add to homescreen for Safari on IOS
        { 'name': 'apple-mobile-web-app-capable', 'content': 'yes' },
        { 'name': 'apple-mobile-web-app-status-bar-style', 'content': 'white' },
        { 'name': 'apple-mobile-web-app-title', 'content': 'dex' },
    ]
};

export default config;
