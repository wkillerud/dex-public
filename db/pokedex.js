import sqlite3 from 'sqlite3';

// The path to this database seems to be relative to the cwd of the node command,
// rather than relative in actual file structure. Go figure...
export const db = new sqlite3.Database('./db/pokedex.sqlite', (err) => {
    if (err) {
        console.log(err);
    }
});