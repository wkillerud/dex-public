# dex

Dex is a mobile-first searchable encyclopedia for Pokemon X & Y and below

![](https://zippy.gfycat.com/ConfusedSimpleFrilledlizard.gif)

[Live demo](https://agile-falls-46975.herokuapp.com/)

Dex is made using Node, Express, and React.
ESLint, Babel, Webpack, Karma, Mocha, and ApiDoc make up the tooling and testing.

## Based on the ground-work made in these example projects

* https://github.com/choonkending/react-webpack-node
* https://github.com/veekun/pokedex
* https://github.com/erikras/react-redux-universal-hot-example

The database required by this application can be downloaded from Veekun:
http://veekun.com/dex/downloads

Other assets can also be found there

From the Veekun Github project README:

```
This software is copyrighted and licensed under the MIT license.  See the file
entitled LICENSE for full copyright and license text.

This software comes bundled with data, sprites, and sounds extracted from the
Pokémon series of video games.  Some terminology from the Pokémon franchise is
also necessarily used within the software itself.  This is all the intellectual
property of Nintendo, Creatures, inc., and GAME FREAK, inc. and is protected by
various copyrights and trademarks.  The author believes that the use of this
intellectual property for a fan reference is covered by fair use and that the
software is significantly impaired without said property included.  Any use of
this copyrighted property is at your own legal risk.

A complete revision history of this software is available from
https://github.com/veekun/pokedex.

Contributors:
Alex Munroe (Eevee): git@veekun.com
```
