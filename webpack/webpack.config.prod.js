var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');

var assetsPath = path.join(__dirname, '..', 'public', 'bundles'); // Location to put the finished bundles
var publicPath = '/bundles/'; // The output path used by the browser to get to the bundles (from www document root)

var commonLoaders = [
  {
    /*
     * TC39 categorises proposals for babel in 4 stages
     * Read more http://babeljs.io/docs/usage/experimental/
     */
    test: /\.js$|\.jsx$/,
    loader: 'babel?presets[]=react,presets[]=es2015,presets[]=stage-0',
    include: path.join(__dirname, '..',  'app')
  },
  { test: /\.json$/, loader: 'json-loader' },
  {
    test: /\.(png|jpg)$/,
    loader: 'url?limit=25000'
  },
  { test: /\.less$/,
    loader: ExtractTextPlugin.extract('style-loader', 'css-loader?!postcss-loader!less?includePaths[]='
      + encodeURIComponent(path.resolve(__dirname, '..', 'app', 'less')))
  }
];

module.exports = [
  {
    // The configuration for the client
    name: 'browser',
    /* The entry point of the bundle
     * Entry points for multi page app could be more complex
     * A good example of entry points would be:
     * entry: {
     *   pageA: './pageA',
     *   pageB: './pageB',
     *   pageC: './pageC',
     *   adminPageA: './adminPageA',
     *   adminPageB: './adminPageB',
     *   adminPageC: './adminPageC'
     * }
     *
     * We can then proceed to optimize what are the common chunks
     * plugins: [
     *  new CommonsChunkPlugin('admin-commons.js', ['adminPageA', 'adminPageB']),
     *  new CommonsChunkPlugin('common.js', ['pageA', 'pageB', 'admin-commons.js'], 2),
     *  new CommonsChunkPlugin('c-commons.js', ['pageC', 'adminPageC']);
     * ]
     */
    // A SourceMap is emitted.
    devtool: 'source-map',
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: './client'
    },
    output: {
      // The output directory as absolute path
      path: assetsPath,
      // The filename of the entry chunk as relative path inside the output.path directory
      filename: '[name].js',
      // The output path used by the browser (www document root)
      publicPath: publicPath

    },
    module: {
      loaders: commonLoaders
    },
    resolve: {
      extensions: ['', '.js', '.jsx', '.less'],
      modulesDirectories: [
        'app', 'node_modules'
      ]
    },
    plugins: [
        // Order the modules and chunks by occurrence.
        // This saves space, because often referenced modules
        // and chunks get smaller ids.
        new webpack.optimize.OccurenceOrderPlugin(),
        // extract inline css from modules into separate files
        new ExtractTextPlugin('main.css'),
        new webpack.optimize.UglifyJsPlugin({
          compressor: {
            warnings: false
          }
        }),
        new webpack.DefinePlugin({
          __TEST__: JSON.stringify(JSON.parse(process.env.TEST_ENV || 'false')),
          __DEV__: false
        })
    ]
  }, {
    // The configuration for the server-side rendering
    name: 'server-side rendering',
    context: path.join(__dirname, '..', 'app'),
    entry: {
      app: './server'
    },
    target: 'node',
    output: {
      // The output directory as absolute path
      path: assetsPath,
      // The filename of the entry chunk as relative path inside the output.path directory
      filename: '[name].server.js',
      // The output path used by the browser (www document root)
      publicPath: publicPath,
      libraryTarget: 'commonjs2'
    },
    module: {
      loaders: commonLoaders
    },
    resolve: {
      extensions: ['', '.js', '.jsx', '.less'],
      modulesDirectories: [
        'app', 'node_modules'
      ]
    },
    plugins: [
        // Order the modules and chunks by occurrence.
        // This saves space, because often referenced modules
        // and chunks get smaller ids.
        new webpack.optimize.OccurenceOrderPlugin(),
        // extract inline css from modules into separate files
        new ExtractTextPlugin('main.css'),
        new webpack.optimize.UglifyJsPlugin({
          compressor: {
            warnings: false
          }
        }),
        new webpack.DefinePlugin({
          __TEST__: JSON.stringify(JSON.parse(process.env.TEST_ENV || 'false')),
          __DEV__: false
        })
    ]
  }
];
